#' Fitting of mixture of von Mises distributions
#'
#' Essentially a wrapper around \code{\link[movMF]{movMF}} to fit a n-component
#' mixture of von Mises distributions to a 1-dimensional circular variable
#'
#' @param x Numeric vector
#' @param k Integer value specifying the number of mixture components to be fitted
#' @param control List of fitting control parameters to be passed on to \code{\link[movMF]{movMF}}
#' @param ... Arguments to be passed to \code{\link[movMF]{movMF}}
#'
#' @return An object of class \code{\link[movMF]{movMF}}
#' @export
#'
#' @examples
#' set.seed(100)
#' x <- c(rnorm(1000, 1, 0.25), rnorm(1000, -1, 0.25))
#' (m <- vM_mix(x))
#' print(class(m))
vM_mix <- function(x, k = 2, control = list(maxiter = 1000), ...) {
    require(movMF)

    # check if circular
    # stopifnot(min(x, na.rm=TRUE) >= -pi & max(x, na.rm=TRUE) <= pi)

    em <- movMF(cbind(cos(x), sin(x)),
                k = k,
                control = control,
                ...)
}


#' Plot histograms and von Mises mixture fit
#'
#' @param x Numeric vector
#' @param model Model fit object of class \code{\link[movMF]{movMF}}
#' @param args.line List of n-element vectors of line parameters to be passed to \code{\link{panel.mathdensity}}
#' @param ... Arguments to be passed to \code{\link[lattice]{histogram}}
#'
#' @return A lattice plot object of class \code{\link[lattice]{trellis.object}}
#' @export
#'
#' @examples
#' \dontrun{
#' set.seed(100)
#' x <- c(rnorm(1000, 1, 0.25), rnorm(1000, -1, 0.25))
#' m <- vM_mix(x)
#' plot_vM(x, m, breaks = 30, col = "grey")
#' }
plot_vM <- function(x, model,
                    args.line = list(col = c("#009E73", "#CC79A7"), lwd = 2, n = 100),
                    ...) {
    require(lattice)
    require(circular)

    mukappa <- theta2mukappa(model$theta)

    distr_fun <- function(x, mu, kappa, alpha) {
        if (!is.circular(x)) {
            x <- circular(x, type = "angles", units = "radians")
        }
        dvonmises(x, 
                  circular(mu, type = "angles", units = "radians"),
                  kappa) * alpha
    }

    nc <- length(model$alpha)
    # stopifnot(length(col.line) == nc)

    histogram(~ x, type = "density", ...,
              panel = function(...) {
                  panel.histogram(...)
                  for (i in seq_along(model$alpha)) {
                      do.call(panel.mathdensity,
			                  c(list(dmath = distr_fun,
                                args = list(mu = mukappa[1, i],
		    			                    kappa = mukappa[2, i],
					                        alpha = model$alpha[i])), 
				                lapply(args.line, function(x, i) {
				                    rep(x, len = nc)[i]
				                }, i)))
                  }
              }
    )
}


#' Convert von Mises parameters from \code{theta} form to \code{mu}-\code{kappa} form
#'
#' @param theta a matrix with rows giving the fitted parameters of the mixture components
#'
#' @return a \code{matrix} with \code{mu} giving the location and \code{kappa} the concentration parameters of the mixture components
theta2mukappa <- function(theta) {
    apply(theta, 1,
        function(x) {
            c(mu = do.call(atan2, as.list(rev(x))),
              kappa = norm(x, type = "2"))
        },
        simplify = TRUE
    )
}
