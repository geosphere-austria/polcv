# Polarimetric change vector analysis for synthetic aperture radar data

This repository supplements the publication by
Stefan Schlaffer <sup>[![orcid logo](https://info.orcid.org/wp-content/uploads/2020/12/orcid_16x16.gif)](https://orcid.org/0000-0003-4742-8648)</sup> and
Matthias Schlögl <sup>[![orcid logo](https://info.orcid.org/wp-content/uploads/2020/12/orcid_16x16.gif)](https://orcid.org/0000-0002-4357-523X)</sup>
(2024): **Snow Avalanche Debris Analysis Using Time Series of Dual-Polarimetric Synthetic Aperture Radar Data**. _IEEE Journal of Selected Topics in Applied Earth Observations and Remote Sensing, 17_, 12567-12578, [10.1109/JSTARS.2024.3423403](https://doi.org/10.1109/JSTARS.2024.3423403).

Users of the package in this repository should cite the above publication.

You can install the `polcv` package like so:

```R
#install.packages("remotes")
remotes::install_gitlab(repo = "geosphere-austria/polcv")
```

### Documentation

Documentation can be found [here](https://geosphere-austria.gitlab.io/polcv/).
