% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/pcv.R
\name{compute_pcv}
\alias{compute_pcv}
\title{Compute polarimetric change vectors from raster files}
\usage{
compute_pcv(x, ...)
}
\arguments{
\item{x}{Matrix of dimension `n x 6` with columns in the following order: VV_{t-1}, VV_{t}, VH_{t-1}, VH_{t}, mask_{t-1}, mask_{t}}

\item{...}{Arguments passed to \code{\link{pcv}}. Currently, only \code{dB} is
implemented.}
}
\value{
Matrix of dimension `n x 2` (PCV magnitude and direction)
}
\description{
PCV wrapper function for use in \code{\link[terra]{app}} or
  \code{\link[raster]{calc}}. The wrapper also takes into account a mask for
  non-valid pixels (e.g. pixels affected by layover and radar shadow).
}
\examples{
\dontrun{
library(terra)
vv1 <- rast(file_vv1)
vv2 <- rast(file_vv2)
vh1 <- rast(file_vh1)
vh2 <- rast(file_vh2)
msk1 <- rast(file_msk1)
msk2 <- rast(file_msk2)

result <- app(x = c(vv1, vv2, vh1, vh2, msk1, msk2), fun = compute_pcv)
}
}
